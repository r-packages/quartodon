
<!-- README.md is generated from README.Rmd. Please edit that file -->

# <img src='man/figures/logo.png' align="right" style="height:250px" height="250" /> quartodon 📦

## Extract and post ‘Mastodon’ threads from text files

<!-- badges: start -->

[![CRAN
status](https://www.r-pkg.org/badges/version/quartodon)](https://cran.r-project.org/package=quartodon)

[![Dependency
status](https://tinyverse.netlify.com/badge/quartodon)](https://CRAN.R-project.org/package=quartodon)

[![Pipeline
status](https://gitlab.com/r-packages/quartodon/badges/main/pipeline.svg)](https://gitlab.com/r-packages/quartodon/-/commits/main)

[![Downloads last
month](https://cranlogs.r-pkg.org/badges/last-month/quartodon?color=brightgreen)](https://cran.r-project.org/package=quartodon)

[![Total
downloads](https://cranlogs.r-pkg.org/badges/grand-total/quartodon?color=brightgreen)](https://cran.r-project.org/package=quartodon)

[![Coverage
status](https://codecov.io/gl/r-packages/quartodon/branch/main/graph/badge.svg)](https://app.codecov.io/gl/r-packages/quartodon?branch=main)

<!-- badges: end -->

The pkgdown website for this project is located at
<https://quartodon.opens.science>. If the development version also has a
pkgdown website, that’s located at
<https://r-packages.gitlab.io/quartodon>.

<!-- - - - - - - - - - - - - - - - - - - - - -->
<!-- Start of a custom bit for every package -->
<!-- - - - - - - - - - - - - - - - - - - - - -->
<!-- - - - - - - - - - - - - - - - - - - - - -->
<!--  End of a custom bit for every package  -->
<!-- - - - - - - - - - - - - - - - - - - - - -->

## Installation

You can install the released version of `quartodon` from
[CRAN](https://CRAN.R-project.org) with:

``` r
install.packages('quartodon');
```

You can install the development version of `quartodon` from
[GitLab](https://about.gitlab.com) with:

``` r
remotes::install_gitlab('r-packages/quartodon');
```

(assuming you have `remotes` installed; otherwise, install that first
using the `install.packages` function)

You can install the cutting edge development version (own risk, don’t
try this at home, etc) of `quartodon` from
[GitLab](https://about.gitlab.com) with:

``` r
remotes::install_gitlab('r-packages/quartodon@dev');
```

<!-- - - - - - - - - - - - - - - - - - - - - -->
<!-- Start of a custom bit for every package -->
<!-- - - - - - - - - - - - - - - - - - - - - -->
<!-- - - - - - - - - - - - - - - - - - - - - -->
<!--  End of a custom bit for every package  -->
<!-- - - - - - - - - - - - - - - - - - - - - -->
