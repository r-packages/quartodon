#' Preview the toots as they would be posted.
#'
#' The function creates an HTML file with a preview of the toots that would
#' be posted if `x` would be passed to [quartodon::post_thread_from_df()]
#' instead.
#'
#' @param x A `toot_df` as produced by a call
#' to [quartodon::prep_and_check_toot_df()]
#' or [quartodon::append_thread_counter()]; or the object produced by a call
#' to [quartodon::split_to_toots()], or the `toot_df` contained in such an
#' object (in `$df`).
#' @param select Optionally, the number(s) of the toot(s) to select to preview.
#' @param embed Whether to only return the HTML code to be included in
#' a `<body>` tag of an existing HTML file. If `embed=TRUE`, nothing is shown
#' in the viewer and no files are written.
#' @param filename The file to write the HTML with the preview to.
#' @param encoding The encoding to use when writing the file.
#'
#' @return Invisibly, the filename in which the HTML is stored.
#' @export
#'
#' @examples ### Get example post directory
#' examplePostDir <-
#'   system.file("example-post",
#'               package = "quartodon");
#'
#' ### Get an example text (see the intro vignette)
#' exampleText <-
#'   readLines(
#'     file.path(examplePostDir, "quartodon.Rmd"),
#'     encoding = "UTF-8"
#'   );
#'
#' ### Extract the toots
#' extractedToots <- quartodon::split_to_toots(
#'   exampleText
#' );
#'
#' ### Append the thread counters
#' toot_df <- quartodon::append_thread_counter(
#'   extractedToots
#' );
#'
#' ### Prepare and check the toots (for length, existing
#' ### images, etc)
#' toot_df <- quartodon::prep_and_check_toot_df(
#'   toot_df,
#'   postsPath = examplePostDir
#' );
#'
#' ### Preview the thread in the viewer
#' previewFile <- quartodon::preview_toot_df(
#'   toot_df
#' );
preview_toot_df <- function(x,
                            select = NULL,
                            embed = FALSE,
                            filename = tempfile(pattern="quartodon_preview",
                                                fileext=".html"),
                            encoding = getOption("quartodon_encoding", "UTF-8")) {

  if (inherits(x, "quartodon_split_toots")) {
    x <- x$df;
  } else if (!inherits(x, "quartodon_toot_df")) {
    stop("As `x`, pass the object produced by `quartodon::split_to_toots()` ",
         "or the `toot_df` dataframe contained in that object (in $df). The ",
         "object you pass must have class 'quartodon_split_toots' or ",
         "'quartodon_toot_df', but it currently has class(es) ",
         vecTxtQ(class(x)), ".");
  }

  if (embed) {
    res <- "";
  } else {
    res <- "<!doctype html>
  <html lang=\"en\">
    <head>
      <meta charset=\"utf-8\">
      <title>quartodon toot_df preview</title>
      <style>
        body {
          font-family: Arial, sans-serif;
        }
      </style>
    </head>
    <body>
      <h1><pre style=\"display:inline\">{quartodon}</pre> toot_df preview</h1>
";
  }

  escapedToots <- gsub("&", "&amp;", x$toots);
  escapedToots <- gsub("<", "&lt;", escapedToots);
  escapedToots <- gsub(">", "&gt;", escapedToots);

  ### Replace newline characters with HTML line breaks
  escapedToots <- gsub("\n", "<br />", escapedToots);

  if (!is.null(select)) {
    if (is.numeric(select)) {
      if (all(select %in% seq_along(escapedToots))) {
        x <- x[select, ];
        escapedToots <- escapedToots[select];
      }
    }
  }



  res <- paste0(
    res,
    paste0(
      paste0(
        "<p>",
        escapedToots,
        "</p><div style=\"margin:10px; padding:5px; border:1px solid blue; ",
        "border-radius: 5px; background-color:#DDF; color:#00F; \">",
        ifelse(nchar(x$error) > 0,
               "\u274C",
               "\u2705"),
        " [nchar=", x$toots_final_nchar,
        ", ",
        ifelse(x$hasImg,
               paste0("img=", x$imgPaths, ", alt=", x$alt, "]"),
               "no image]"),
        "</div>",
        ifelse(nchar(x$error) > 0,
               paste0("<div style=\"margin:10px; padding:5px; border:1px solid red; ",
                      "border-radius: 5px; background-color:#FDD; color:#F00;\">\u274C ",
                      x$error, "</div>"),
               ""),
        "\n\n<hr />\n\n"
      ),
      collapse="\n"
    )
  );

  if (embed) {
    res <- c(res, "");
  } else {
    res <- c(res,
           "
    </body>
  </html>");
  }

  if (embed) {
    return(
      structure(
        res,
        class = "knit_asis",
        knit_meta = NULL,
        knit_cacheable = NA
      )
    );
  } else {

    writeTxtFile(
      res,
      filename,
      encoding = encoding
    );

    if ((requireNamespace("rstudioapi", quietly = TRUE)) &&
        (rstudioapi::isAvailable())) {
      viewer <- rstudioapi::viewer;
    } else {
      viewer <- getOption("viewer",
                          FALSE);
    }

    if (is.function(viewer)) {
      viewer(filename);
    }

    return(invisible(filename));
  }

}
