#' Split a plain text file into toots
#'
#' This function takes a plain text file (such as a Quarto blog post) and splits
#' it into toots, does some cleaning, and returns an object with a data frame
#' and some intermediate products.
#'
#' @param x The plain text file as a character vector.
#' @param fragmentsToSkip The number of fragments to skip when reading
#' the text file (Quarto post, R Markdown file, etc). By default, the first
#' fragment (i.e. the lines preceding the toot separator specified in
#' `tootSeparator`, by default the first five dashes, `-----`) will be skipped.
#' @param tootSeparator The separator that is used to split the file into toots:
#' matched against every line (i.e. element of the character vector).
#' @param preprocess A list of 2-element vectors specifying the preprocessing
#' to perform on each extracted toot. These two argument are the first two
#' arguments to a call to [gsub()], with the toot as the third argument, and
#' `perl = TRUE`.
#' @param imgRegex The regular expression used to find images. It should
#' have one capturing group that extracts the path to the image.
#' @param imgAltRegex The regular expression used to find the images' alt text;
#' it should have one capturing group that extracts the alt text.
#' @param urlRegex The regular expression used to find hyperlinks. It should
#' have one capturing group that extracts the title (not the URL).
#' @param cleanWhitespace Whether to clean white space. If `TRUE`, all newline
#' characters (`\n`) are stripped from the beginning and end of each toot, and
#' all sequences of more than two newline characters are replaced with exactly
#' two newline characters.
#'
#' @return An object with a data frame and some intermediate products.
#' @export
#'
#' @examples ### Get example post directory
#' examplePostDir <-
#'   system.file("example-post",
#'               package = "quartodon");
#'
#' ### Get an example text (see the intro vignette)
#' exampleText <-
#'   readLines(
#'     file.path(examplePostDir, "quartodon.Rmd"),
#'     encoding = "UTF-8"
#'   );
#'
#' ### Extract the toots
#' extractedToots <- split_to_toots(
#'   exampleText
#' );
#'
#' ### Look at the text of the third extracted toot:
#' cat(extractedToots$df$toots[1]);
split_to_toots <- function(x,
                           fragmentsToSkip = getOption("quartodon_fragmentsToSkip",
                                                       1),
                           tootSeparator = getOption("quartodon_tootSeparator",
                                                     "^-----\\s*$"),
                           preprocess = getOption("quartodon_preprocess",
                                                  list(c("^#.*", ""),
                                                       c("`", ""))),
                           imgRegex = getOption("quartodon_imgRegex",
                                                "^!\\[([^\\]]*)\\]\\(([^\\)]*)\\)\\{?([^}]*)\\}?$"),
                                                # r"(^!\[([^\]]*)\]\(([^\)]*)\)\{?([^}]*)\}?$)"),
                           imgAltRegex = getOption("quartodon_imgAltRegex",
                                                   "fig-alt=\"([^\"]*)\""),
                           urlRegex = getOption("quartodon_urlRegex",
                                                "(?!\\!)\\[([^\\]]*)\\]\\(([^\\)]*)\\)({[^}]*})?"),
                                                # r"((?!\!)\[([^\]]*)\]\(([^\)]*)\)\{?([^}]*)\}?)"),
                           cleanWhitespace = getOption("quartodon_urlRegex",
                                                       TRUE)) {

  ###---------------------------------------------------------------------------
  ### Split the character vector into toots
  ###---------------------------------------------------------------------------

  sepLocations <- grep(tootSeparator, x);
  tootStarts <- grep(tootSeparator, x) + fragmentsToSkip;
  tootEnds <- c(sepLocations[-fragmentsToSkip] - 1, length(x));
  tootLines <- mapply(`:`, tootStarts, tootEnds, SIMPLIFY = FALSE);
  toots_raw <- lapply(tootLines, function(i) return(x[i]));

  ###---------------------------------------------------------------------------
  ### Perform preprocessing steps (e.g. remove headings)
  ###---------------------------------------------------------------------------

  toots_preprocessed <- toots_raw;

  ### Only retain toots that have content other than whitespace
  toots_preprocessed <-
    toots_preprocessed[
      unlist(lapply(
        toots_preprocessed,
        function(tootVector) {
          grepl(
            "\\S+",
            paste0(tootVector, collapse="")
          )
        }
      ))
    ];

  if (!is.null(preprocess)) {
    for (currentStep in preprocess) {
      toots_preprocessed <-
        lapply(
          toots_preprocessed,
          gsub,
          pattern = currentStep[1],
          replacement = currentStep[2],
          perl = TRUE
        );
    }
  }

  ###---------------------------------------------------------------------------
  ### Get image info and remove image information from toots
  ###---------------------------------------------------------------------------

  imgInfo <-
    lapply(
      toots_preprocessed,
      regex_captures,
      pattern = imgRegex
    );

  imgImgLine <-
    lapply(
      imgInfo,
      function(x) {
        return(which(unlist(lapply(x, length)) > 0));
      }
    );

  imgImgURL <-
    lapply(
      seq_along(imgInfo),
      function(i) {
        if (length(imgImgLine[[i]]) > 0) {
          return(imgInfo[[i]][[imgImgLine[[i]][1]]][3]);
        } else {
          return("");
        }
      }
    );

  imgImgAlt <-
    lapply(
      seq_along(imgInfo),
      function(i) {
        if (length(imgImgLine[[i]]) > 0) {
          x <- imgInfo[[i]][[imgImgLine[[i]][1]]][4];
          altTexts <-
            regmatches(
              x,
              regexec(
                imgAltRegex,
                x
              )
            )[[1]][2]
          return(
            ifelse(
              is.na(altTexts),
              "",
              altTexts
            )
          );
        } else {
          return("");
        }
      }
    );

  toots_without_images <-
    lapply(
      seq_along(toots_preprocessed),
      function(i) {
        res <- toots_preprocessed[[i]];
        if (length(imgImgLine[[i]]) > 0) {
          res[imgImgLine[[i]]] <- "";
        }
        return(res);
      }
    );

  ###---------------------------------------------------------------------------
  ### Paste toots together into one character value
  ###---------------------------------------------------------------------------

  toots_pasted <-
    unlist(
      lapply(
        toots_without_images,
        paste,
        sep = "",
        collapse = "\n"
      )
    );

  ###---------------------------------------------------------------------------
  ### Clean up whitespace
  ###---------------------------------------------------------------------------

  if (cleanWhitespace) {
    toots_cleaned_ws <- gsub("^\n*", "", toots_pasted);
    toots_cleaned_ws <- gsub("\n*$", "", toots_cleaned_ws);
    toots_cleaned_ws <- gsub("\n\n+", "\n\n", toots_cleaned_ws);
  } else {
    toots_cleaned_ws <- toots_pasted;
  }

  ###---------------------------------------------------------------------------
  ### Get URL info and repace URLs
  ###---------------------------------------------------------------------------

  urlInfo <-
    regex_captures(
      urlRegex,
      toots_cleaned_ws
    );

  toots_removed_urls <-
    gsub(
      urlRegex,
      "\\1",
      toots_cleaned_ws,
      perl = TRUE
    );

  ###---------------------------------------------------------------------------
  ### Compose object to return and return it
  ###---------------------------------------------------------------------------

  res <- list(
    toots_raw = toots_raw,
    intermediate =
      list(
        toots_preprocessed = toots_preprocessed,
        imgInfo = imgInfo,
        imgImgLine = imgImgLine,
        imgImgURL = imgImgURL,
        imgImgAlt = imgImgAlt,
        toots_without_images = toots_without_images,
        toots_pasted = toots_pasted,
        toots_cleaned_ws = toots_cleaned_ws,
        urlInfo = urlInfo,
        toots_removed_urls = toots_removed_urls
      ),
    df =
      data.frame(
        toots_preprocessed = toots_removed_urls,
        toots = toots_removed_urls,
        img = unlist(imgImgURL),
        alt = unlist(imgImgAlt)
      )
  );

  class(res$df) <- c("quartodon_toot_df", class(res$df));
  class(res) <- c("quartodon_split_toots", class(class(res)));

  return(res);

}
