#' Create an embeddable fragment
#'
#' This function takes a Mastodon toot identifier, a Mastodon instance, and
#' a Mastodon username to create a fragment that can be embedded in a Markdown
#' file.
#'
#' @param x A Mastodon toot id
#' @param tokenname The name of the environment variable containing the {rtoot}
#' token produced with [rtoot::auth_setup()] and [rtoot::convert_token_to_envvar()].
#' @param instance A Mastodon instance
#' @param username A Mastodon username
#'
#' @return A character vector.
#' @export
#'
#' @examples cat(
#'   quartodon::create_embed_fragment(
#'     "109445312966461935",
#'     "mastodon.nl",
#'     "matherion"
#'   )
#' );
create_embed_fragment <- function(x,
                                  tokenname = getOption("quartodon_tokenname",
                                                        "RTOOT_DEFAULT_TOKEN"),
                                  instance = getOption("quartodon_instance",
                                                       NULL),
                                  username = getOption("quartodon_username", FALSE)) {

  if (identical(FALSE, username)) {
    warning("\n\nTo create this embed fragment, I need to know your username. ",
            "Store it in an option named 'quartodon_username' using the ",
            "'option()' function. For example:\n\n",
            "  option(quartodon_username = 'matherion');\n\n",
            "Alternatively, you can also rerun this function ",
            "and specify the username yourself:\n\n",
            "  quartodon::create_embed_fragment(\n",
            "    ", x, "\n",
            "    username = 'YOUR_USERNAME_HERE'\n",
            "  );\n\n",
            "For now, using 'YOUR_USERNAME_HERE' as username in the fragment ",
            "I generate, so you will have to replace ",
            "that manually yourself, sorry! (It occurs twice, don't forget to ",
            "replace both.)\n");
    username <- "YOUR_USERNAME_HERE";
  }

  if (is.null(instance)) {

    rTootToken <- Sys.getenv(tokenname);

    if (is.character(rTootToken)) {
      if (!grepl(";user;", rTootToken)) {
        stop("I found a token stored in the environment variable you specified (",
             tokenname, "), but it does not appear to be a valid {rtoot} token ",
             "a user. Please create the {rtoot} token using {rtoot::auth_setup()} ",
             "with 'type = \"user\"', and use {rtoot::convert_token_to_envvar()} ",
             "to convert the token to an environment variable and store it. See ",
             "the {rtoot} help pages for more information.");
      }
    } else {
      stop("No valid {rtoot} token found in the specified environment variable (",
           tokenname, "). If you stored the {rtoot} token in an environment ",
           "variable with a different name, please store that name using:\n\n",
           "  options(quartodon_tokenname=\"RTOOT_DEFAULT_TOKEN\");\n\n",
           "(Replace \"RTOOT_DEFAULT_TOKEN\" with the name you want to use.)\n\n",
           "If you didn't create an environment variable with a token yet, you ",
           "can it using the {rtoot} package using {rtoot::auth_setup()} ",
           "with 'type = \"user\"', and use {rtoot::convert_token_to_envvar()} ",
           "to convert the token to an environment variable and store it. See ",
           "the {rtoot} help pages for more information.");
    }

    instanceFromToken <-
      gsub(".*;user;", "", rTootToken);
    if (nchar(instanceFromToken) > 0) {
      instance <- instanceFromToken;
    } else {

      warning("\n\nI failed to derive a Mastodon instance from the {rtoot} ",
              "token stored in the environment variable you passed. Please ",
              "check your token, or you can rerun this function ",
              "and specify the instance yourself:\n\n",
              "  quartodon::create_embed_fragment(\n",
              "    ", x, "\n",
              "    username = '", username, "'\n",
              "    instance = 'YOUR.INSTANCE.HERE'\n",
              "  );\n\n",
              "For now, using 'YOUR.INSTANCE.HERE' as instance, so you will ",
              "have to replace that manually yourself, sorry! (It occurs ",
              "twice, don't forget to replace both.)\n");
      instance <- "YOUR.INSTANCE.HERE";
    }

  }

  return(
    paste0(
      "\n\nThis is a Mastodon thread. ",
      "[The original thread is available here]",
      "(https://", instance, "/@", username, "/", x,
      "){.external target=\"_blank\"}:",
      "\n\n",
      "<iframe src=\"https://", instance, "/@", username, "/", x, "/embed\" ",
      "class=\"mastodon-embed\" style=\"max-width: 100%; border: 0\" ",
      "width=\"400\" allowfullscreen=\"allowfullscreen\"></iframe>",
      "<script src=\"https://", instance,
      "/embed.js\" async=\"async\"></script>\n\n"
    )
  );

}
