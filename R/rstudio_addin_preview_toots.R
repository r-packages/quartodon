#' An RStudio plugin to preview the toots in a document
#'
#' This function extracts all toots in the active document and previews them.
#'
#' @return Invisibly, the document with results.
#' @export
rstudio_addin_preview_toots <- function() {

  if (!requireNamespace("rstudioapi", quietly = TRUE)) {
    stop("To use this plugin function, you need to have the {rstudioapi} ",
         "package installed! You can install it with:\n\n",
         "  install.packages('rstudioapi');");
  }

  activeDoc <- rstudioapi::getActiveDocumentContext();

  docContents <- activeDoc$contents;
  docPath <- dirname(activeDoc$path);

  if ((all(nchar(docContents) == 0)) && all(nchar(docPath) == 0)) {
    if (activeDoc$id == "#console") {
      stop("The cursor is not in a document in the 'source' pane but in the ",
           "console.");
    } else {
      stop("The cursor is not in a document in the 'source' pane, or the ",
           "document is empty.");
    }
  }

  splitToots <- split_to_toots(docContents);

  tootDf <- append_thread_counter(splitToots);
  tootDf <- prep_and_check_toot_df(tootDf,
                                   postsPath = docPath,
                                   stopOnErrors = FALSE);

  previewResults <- preview_toot_df(tootDf);

  return(invisible(previewResults));

}
